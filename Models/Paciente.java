package Models;

public class Paciente extends Usuario {
	
	private String CadastroPaciente;
	private String PlanoDeSaude;
	private String NumeroFila;
	private Consulta consulta;
	private String medicamento;
	
	public Paciente (String Nome){
		super(Nome);	
	}
	
	
	public String getCadastroPaciente() {
		return CadastroPaciente;
	}
	public void setCadastroPaciente(String cadastroPaciente) {
		CadastroPaciente = cadastroPaciente;
	}
	public String getPlanoDeSaude() {
		return PlanoDeSaude;
	}
	public void setPlanoDeSaude(String planoDeSaude) {
		PlanoDeSaude = planoDeSaude;
	}
	public String getNumeroFila() {
		return NumeroFila;
	}
	public void setNumeroFila(String numeroFila) {
		NumeroFila = numeroFila;
	}


	public Consulta getConsulta() {
		return consulta;
	}


	public void setConsulta(Consulta consulta) {
		this.consulta = consulta;
	}


	public String getMedicamento() {
		return medicamento;
	}


	public void setMedicamento(String medicamento) {
		this.medicamento = medicamento;
	}

	

}
